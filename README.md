<!--
SPDX-FileCopyrightText: 2024 Jordi Estrada

SPDX-License-Identifier: MIT
-->

# wbmail

Waybar custom module to check for new email messages.

## Getting started

Install [uv](https://github.com/astral-sh/uv). Uv is a Python package installer and resolver:

```shell
# On Linux.
curl -LsSf https://astral.sh/uv/install.sh | sh

# With Pacman.
pacman -S uv
```

To create a virtual environment:

```shell
uv venv  # Create a virtual environment at .venv.
```

To activate the virtual environment:

```shell
# On bash.
source .venv/bin/activate

# On nushell.
overlay use .venv/bin/activate.nu
```

To install a package into the virtual environment:

```shell
uv pip install ./wbmail-0.1.0-py3-none-any.whl  # Install wbmail.
```

## Configuration

Run `wbmail -h` for usage:

```
usage: wbmail [-h] [-i] server user

Check email server for new messages

positional arguments:
  server      IMAP mail server name
  user        User name

options:
  -h, --help  show this help message and exit
  -i, --init  Encrypt password
```

You must run `wbmail --init SERVER USER` anytime the email account password changes. It encrypts and stores the password in a `.key` file.

```
.
├── .key
└── __init__.py
```

Add to ~/.config/waybar/config.jsonc:

```json
"modules-right": ["custom/mail"],

"custom/mail": {
    "format": " {} {icon}",
    "return-type": "json",
    "format-icons": {
        "new-messages": "<b></b> ",
        "no-new-messages": " "
    },
    "exec": "wbmail SERVER USER",
    "exec-on-event": false,
    "escape": true,
    "on-click": "MOZ_ENABLE_WAYLAND=1 thunderbird"
}
```

Install Nerd Fonts or Font Awesome to display icons:

```shell
# With Pacman.
pacman -S otf-font-awesome
```

* License: MIT
