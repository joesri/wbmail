# SPDX-FileCopyrightText: 2024 Jordi Estrada
#
# SPDX-License-Identifier: MIT

# Monitor Inbox for new messages.
# To exec within a waybar custom module

import email
import argparse
import tomlkit
from imapclient import IMAPClient
from email import policy
from cryptography.fernet import Fernet
from pathlib import Path
from tomlkit.toml_file import TOMLFile


def format_out(text, tooltip):
    # The waybar module expects the script to output its data in json format like
    # this: {"text": "$text", "tooltip": "$tooltip", "class": "$class",
    #        "alt": "$alt"}
    if text > 0:
        out = f'{{"text": "{text}", "tooltip": "{tooltip}", "class": "new-messages", "alt": "new-messages"}}'
    else:
        out = f'{{"text": "{text}", "tooltip": "No new messages", "class": "no-new-messages", "alt": "no-new-messages"}}'
    return out


def get_messages(imap):
    tip = ""
    messages = imap.search("UNSEEN")
    unread = len(messages)
    for uid, message_data in imap.fetch(messages, "RFC822").items():
        email_message = email.message_from_bytes(
            message_data[b"RFC822"], policy=policy.default
        )
        sender = email_message.get("From").split("<", 1)[0].replace('"', "'") + "> "
        subject = email_message.get("Subject").replace("'", "'").replace('"', "'")
        tip += sender + subject + "\\n"
    tip = tip[:-2]
    return unread, tip


def save_password(server, user, path):
    # Encrypt the password
    key = Fernet.generate_key().decode()
    fernet = Fernet(key)
    new_pwd = input("New password: ")
    new_encpwd = fernet.encrypt(new_pwd.encode()).decode()
    account = f"{server}:{user}"
    # Write a new TOML file from scratch
    config = tomlkit.document()
    config["key"] = key
    config[account] = new_encpwd
    TOMLFile(path).write(config)


def update_password(config, server, user, path):
    # Encrypt the password
    key = config["key"]
    fernet = Fernet(key)
    new_pwd = input("New password: ")
    new_encpwd = fernet.encrypt(new_pwd.encode()).decode()
    # Update the key file
    account = f"{server}:{user}"
    config[account] = new_encpwd
    TOMLFile(path).write(config)


def main() -> int:
    # Parse the program arguments
    parser = argparse.ArgumentParser(description="Check email server for new messages")
    parser.add_argument("server", help="IMAP mail server name")
    parser.add_argument("user", help="User name")
    parser.add_argument("-i", "--init", help="Encrypt password", action="store_true")
    args = parser.parse_args()

    server = args.server
    user = args.user
    path = Path(__file__).parent / ".key"

    # Write or update the key file
    if args.init:
        if not path.is_file():
            save_password(server, user, path)
        else:
            config = TOMLFile(path).read()
            if "key" in config:
                update_password(config, server, user, path)
            else:
                save_password(server, user, path)

    # Connect to the server and get the messages
    try:
        # Get the encrypted password
        config = TOMLFile(path).read()
        key = config["key"]
        encpwd = config[server + ":" + user]
        # Decrypt the password
        assert key
        fernet = Fernet(key)
        pwd = fernet.decrypt(encpwd.encode()).decode()
        # Login to the server
        imap = IMAPClient(server)
        imap.login(user, pwd)
        imap.select_folder("INBOX", readonly=True)
        # Output the current unread messages
        unread, tip = get_messages(imap)
        print(format_out(unread, tip), flush=True)
        # Monitor inbox for new messages
        while True:
            # Start IDLE mode
            try:
                imap.idle()
                # print("Connection is now in IDLE mode, send yourself an email or quit with ^c")
                # Wait for up to 30 seconds for an IDLE response
                responses = imap.idle_check(timeout=30)
                imap.idle_done()
                if responses:
                    unread, tip = get_messages(imap)
                    print(format_out(unread, tip), flush=True)
                else:
                    pass
            except KeyboardInterrupt:
                break
        # Clean up connection
        imap.idle_done()
        imap.logout()

    except (IOError, KeyError, IMAPClient.Error) as error:
        match error:
            case IOError():
                print(
                    f"Error: config file does not exist. Try to run wbmail again with the --init option. {error}"
                )
            case KeyError():
                print(
                    f"Error: some config missing. Try to run wbmail again with the --init option. {error}"
                )
            case IMAPClient.Error():
                print(f"Error: failed to invalid credentials. {error}")
