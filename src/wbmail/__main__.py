# SPDX-FileCopyrightText: 2024 Jordi Estrada
#
# SPDX-License-Identifier: MIT

import wbmail
import sys

sys.exit(wbmail.main())
